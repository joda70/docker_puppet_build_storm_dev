# Based on the centos latest stable image
FROM joda70/puppet_build_base_env_centos6:latest
MAINTAINER Elisabetta Ronchieri <elisabetta.ronchieri@cnaf.infn.it>

# Get puppet modules
ADD get_puppet_common_modules.sh /root/get_puppet_common_modules.sh
RUN /root/get_puppet_common_modules.sh

# Install packages to develop code
RUN mkdir -p /etc/puppet/modules/puppet-storm-frontend-server-build-deps/manifests
ADD puppet-storm-frontend-server-build-deps/manifests/init.pp /etc/puppet/modules/puppet-storm-frontend-server-build-deps/manifests/
ADD puppet_build_storm_frontend_server_dev_centos.sh /root/puppet_build_storm_frontend_server_dev.sh
RUN /root/puppet_build_storm_frontend_server_dev.sh

RUN yum clean all
