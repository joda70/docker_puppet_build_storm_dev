# == Class: puppet-storm-frontend-server-build-deps
#
# Install building dependencies for the storm-frontend-server package.
# 
# === Examples
#
#   include puppet-storm-frontend-server-build-deps



class puppet-storm-frontend-server-build-deps {

  include puppet-base-build-env
  require puppet-yum-utils
  require puppet-storm-repos

  package { 'autoconf':
    ensure => present
  }

  package { 'libtool':
    ensure => present
  }
  
  $packages = ['storm-frontend-server']

  puppet-yum-utils::builddep { $packages: }
}

