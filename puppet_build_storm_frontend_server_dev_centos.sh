#!/bin/bash

echo "Updating system..."
yum update -y

echo "Applying storm-frontend-server modules..."
MODULES_DIR="/etc/puppet/modules"

puppet apply --modulepath=$MODULES_DIR -e "include puppet-test-ca"
puppet apply --modulepath=$MODULES_DIR -e "include puppet-storm-frontend-server-build-deps"
